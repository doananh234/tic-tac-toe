import { FC, useState } from 'react';
import Block from 'components/Block';
import { BlockValue } from 'types';
import { calculateWinner } from 'tools';

const style = {
  display: 'flex',
  flexDirection: 'column' as const,
  alignItems: 'center',
  justifyContent: 'center',
  margin: 'auto',
  minHeight: '100vh',
};

const boardStyle = {
  background: 'white',
  width: 96,
  border: '1px solid #d1d1d1',
  height: 96,
  display: 'flex',
  flexWrap: 'wrap' as const,
};

const BoardGame: FC = () => {
  const [board, setBoard] = useState<BlockValue[]>(Array(9).fill(null));
  const [xIsNext, setXIsNext] = useState<boolean>(true);
  const winner = calculateWinner(board);

  const onRestart = () => {
    setBoard(Array(9).fill(null));
    setXIsNext(true);
  };

  const onClickBlock = (index: number) => {
    if (winner) return;
    setBoard((prevBoard: BlockValue[]): BlockValue[] => {
      const newBoard = [...prevBoard];
      newBoard[index] = xIsNext ? 'X' : 'O';
      return newBoard;
    });
    setXIsNext((e: boolean) => !e);
  };

  return (
    <div style={style}>
      <div>
        <button type="button" onClick={onRestart}>
          Restart
        </button>
        <br />
        <br />
        <b>{`Turn of: ${xIsNext ? 'X' : 'O'}`}</b>
        <br />
        <br />
      </div>
      <div style={boardStyle}>
        {board.map((block, i) => (
          <Block
            key={String(i)}
            index={i}
            value={block}
            onClick={onClickBlock}
          />
        ))}
      </div>
      <br />
      {!!winner && winner !== 'Draw' && <b>{`Winner: ${winner}`}</b>}
      {winner === 'Draw' && <b>Draw</b>}
    </div>
  );
};

export default BoardGame;
