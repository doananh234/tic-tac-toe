import { FC, memo } from 'react';
import { BlockValue } from 'types';

const style = {
  background: 'white',
  border: '1px solid #d1d1d1',
  fontSize: '30px',
  cursor: 'pointer',
  outline: 'none',
  width: 30,
  height: 30,
  textAlign: 'center' as const,
};

interface BlockProps {
  onClick: (i: number) => void;
  index: number;
  value: BlockValue;
}

const Block: FC<BlockProps> = ({ onClick, index, value }: BlockProps) => {
  return (
    <div
      role="presentation"
      onClick={() => !value && onClick(index)}
      style={style}
    >
      {value}
    </div>
  );
};

export default memo(Block);
