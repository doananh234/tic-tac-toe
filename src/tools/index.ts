import { BlockValue } from 'types';

export const calculateWinner = (squares: BlockValue[]): BlockValue | 'Draw' => {
  const lines: number[][] = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];

  let isFinish = true;

  for (let i = 0; i < lines.length; i += 1) {
    const [a, b, c] = lines[i];
    isFinish = isFinish && !!squares[a] && !!squares[b] && !!squares[c];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }

  return isFinish ? 'Draw' : null;
};

export default { calculateWinner };
